from django.db import models
from django.utils import timezone
from datetime import datetime, date

class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length=30)
    nama_dosen = models.CharField(max_length=30)
    jumlah_sks = models.CharField(max_length=10)
    deskripsi_matkul = models.CharField(max_length=30)
    smt_tahun = models.CharField(max_length=30)
    ruang_kelas = models.CharField(max_length=30)

    def __str__(self):
        return self.nama_matkul

#    phone_number = models.CharField(max_length=17)
#    id_line = models.CharField(max_length=6)

#class Post(models.Model):
#    author = models.ForeignKey(Person, on_delete =   
#      models.CASCADE)
#   content = models.CharField(max_length=125)
#    published_date =   
#      models.DateTimeField(default=timezone.now)


